﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="form.aspx.cs" Inherits="banktake2.WebForm9" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <asp:Panel ID="Panel1" runat="server">
        Please fill up the details below and we will get in touch with you shortly.
          <div><table id="Table1" cellspacing="8" runat="server">
                <tbody>
                    <tr>
                        <td class="Label" style="width: 129px">Account Type</td>
                        <td>
                            <asp:DropDownList ID="DropDownList5" runat="server">
                                <asp:ListItem>savings</asp:ListItem>
                                <asp:ListItem>salary</asp:ListItem>
                                <asp:ListItem>current</asp:ListItem>
                                <asp:ListItem>customer</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">Title <font color="red">*</font> :</td>
                        <td>
                            <asp:DropDownList ID="DropDownListTitle" runat="server">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                                <asp:ListItem>Dr.</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">First Name <font color="red">*</font> :</td>
                        <td><asp:TextBox ID="TextBoxFname" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxFname"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">Middle Name <font color="red">*</font> :</td>
                        <td><asp:TextBox ID="TextBoxMname" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxMname"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">Last Name <font color="red">*</font> :</td>
                        <td><asp:TextBox ID="TextBoxLname" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxLname"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">Address :</td>
                        <td><asp:TextBox ID="TextBoxAddress" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 129px">Pin/Zip <font color="red">*</font> :</td>
                        <td><asp:TextBox ID="TextBoxPin" MaxLength="6" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxPin"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="Number Only" ForeColor="#FF3300" ControlToValidate="TextBoxPin"></asp:RegularExpressionValidator>
                            
                        </td>
                    </tr>
                      <tr>
                <td style="width: 129px">
                    Country</td>
                <td style="width: 245px">
                    <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True" >
                        <asp:ListItem>select</asp:ListItem>                                   
                          </asp:DropDownList>
                </td>
            </tr>
                    <tr>
                        <td style="width: 129px">State :</td>
                        <td style="width: 245px">
                            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                                <asp:ListItem>select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 129px">City :</td>
                        <td style="width: 245px">
                            <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                <asp:ListItem>select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 129px">Branch :</td>
                        <td style="width: 245px">
                            <asp:DropDownList ID="DropDownList4" runat="server" style="margin-bottom: 0px">
                                <asp:ListItem>select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>
         </div>
     <div class="FormTableTwo">
            <table id="Table2" cellspacing="8" runat="server">
                <tbody>
                    <tr>
                        <td class="Label" style="width: 128px">Gender <font color="red">*</font> :</td>
                        <td style="width: 249px"><asp:DropDownList ID="DropDownListGender" runat="server">
                                <asp:ListItem Value="">Select Gender</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="DropDownListGender"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 128px">Marital status<font color="red">*</font> :</td>
                        <td style="width: 249px"><asp:DropDownList ID="DropDownListMaritalStatus" runat="server">
                                <asp:ListItem Value="">Select..</asp:ListItem>
                                <asp:ListItem Value="Single">Single</asp:ListItem>
                                <asp:ListItem Value="Married">Married</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="DropDownListMaritalStatus"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 128px">Date of birth <font color="red">*</font> :</td>
                        <td style="width: 249px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" Enabled="True" TargetControlID="TextBox1">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 128px">E-Mail ID <font color="red">*</font> :</td>
                        <td style="width: 249px"><asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxMobile"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                ErrorMessage="Invailid Email" ForeColor="#FF3300" 
                                ControlToValidate="TextBoxEmail" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" style="width: 128px">Mobile No <font color="red">*</font> :</td>
                        <td style="width: 249px"><asp:TextBox ID="TextBoxMobile" MaxLength="11" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="#FF3300" ControlToValidate="TextBoxMobile"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="Number Only" ForeColor="#FF3300" ControlToValidate="TextBoxMobile"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="Label" style="width: 128px">
                            <asp:TextBox ID="TextBox2" runat="server" OnTextChanged="TextBox2_TextChanged" Visible="False"></asp:TextBox></td>
                        <td style="width: 249px">
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </td>

                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div class="FormButton"><asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
            <asp:Label ID="Label1" runat="server"></asp:Label>
         </div>
    </div>
    </asp:Panel>
    </asp:Content>

